﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Searchfileidbyfilename
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Typ filenaam plz");
            searchidbyfilename(Console.ReadLine());
        }


        static void searchidbyfilename(string filename)
        {
            string SearchId = "SELECT files.id FROM files WHERE files.path LIKE @parameter"; //selecteert alle file ids waarvan de bestandsnaam het gezochte woord of de gezochte letters bevat.
            using (SQLiteConnection connect = new SQLiteConnection(@"Data Source = gevulde_testdatabase.sqlite3; Version = 3"))
            using (SQLiteCommand fileIdCommand = new SQLiteCommand(SearchId, connect))
            {
                connect.Open();
                fileIdCommand.Parameters.Add(new SQLiteParameter("@parameter", "%" + filename + "%"));
                SQLiteDataReader fileIdReader = fileIdCommand.ExecuteReader();
                while (fileIdReader.Read())
                {
                    Console.WriteLine(fileIdReader["id"]);
                }
                Console.ReadLine();
            }
        }


    }
}

